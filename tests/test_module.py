# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class PartyAddressProvinceTestCase(ModuleTestCase):
    """Test Party Address Province module"""
    module = 'party_address_province'


del ModuleTestCase
