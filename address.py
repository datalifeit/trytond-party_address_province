# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class Address(metaclass=PoolMeta):
    __name__ = 'party.address'

    province = fields.Many2One('country.subdivision',
        'Province',
        domain=[
            ('parent', '!=', None),
            ('parent', '=', Eval('subdivision', -1))],
        states={'readonly': ~Eval('active')})

    def _get_address_substitutions(self):
        substitutions = super(Address, self)._get_address_substitutions()
        province = {
            'province': (self.province.name
                if getattr(self, 'province', None) else ''),
            'province_code': (self.province.code
                if getattr(self, 'province', None) else ''),
        }
        substitutions.update(province)
        for key, value in list(province.items()):
            substitutions[key.upper()] = value.upper()
        return substitutions
